# Synopsis XML

See the `data` folder for the **Synopsis Sample** `synopsis.0.0.3.xml`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="../schema/synopsis.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="../schema/synopsis.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/css" href="../assets/css/synopsis.css" title="synopsis" alternate="yes"?>
<synopsis xmlns="https://sub.uni-goettingen.de/met/standards/synopsis#">
    <texts>
        <text xml:id="text-1" href="samples/text-1.xml">
            <title>Das Lorem Ipsum (Göttinger Version)</title>
            <xenoData xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dct="http://purl.org/dc/terms/">
                <dc:alternative>Das Lorem</dc:alternative>
            </xenoData>
        </text>
        <text xml:id="text-2" href="samples/text-2.xml">
            <title>Das Lorem Ipsum (Hamburger Version)</title>
        </text>
        <text xml:id="text-3" href="samples/text-3.xml">
            <title>Das Lorem Ipsum (Berliner Version)</title>
        </text>
        <text xml:id="text-4" href="samples/text-4.xml">
            <title>Das Lorem Ipsum (Goslarer Version)</title>
        </text>
    </texts>
    <groups>
        <group n="Vers 1">
            <item text="text-1" ref="a1"/>
            <item text="text-2" ref="a1"/>
            <item text="text-3" ref="a1"/>
            <item text="text-4" ref="a1"/>
        </group>
        <group n="Vers 2">
            <item text="text-1" ref="a2"/>
            <item text="text-2" ref="a2"/>
            <item text="text-4" ref="a2"/>
        </group>
        <group n="Vers 3">
            <item text="text-1" ref="a3"/>
            <item text="text-2" ref="a3"/>
            <item text="text-3" ref="a3"/>
        </group>
        <group n="Vers 4">
            <item text="text-2" ref="a4"/>
            <item text="text-3" ref="a4"/>
        </group>
        <group n="Vers 5">
            <item text="text-1" ref="a5"/>
            <item text="text-2" ref="a5"/>
            <item text="text-3" ref="a5"/>
            <item text="text-4" ref="a5"/>
        </group>
        <group n="Vers 6">
            <item text="text-1" ref="a6"/>
            <item text="text-2" ref="a6"/>
            <item text="text-3" ref="a6"/>
            <item text="text-4" ref="a6"/>
        </group>
    </groups>
</synopsis>


```

![](docs/img/synopsis0_0_2-html.png)