<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:syn="https://sub.uni-goettingen.de/met/standards/synopsis#"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    exclude-result-prefixes="xs"
    version="2.0" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/synopsis#"
    xmlns="https://www.w3.org/1999/xhtml/">
    
    <xsl:output indent="yes"/>
    
    <xsl:variable name="text-nodes" select="//texts/text"/>
    <xsl:variable name="texts" select="data(//texts/text/@href)"/>
    <xsl:variable name="base-uri" select="base-uri()"/>
    
    <xsl:template match="/">
        <xsl:variable name="timestamp" select="current-dateTime()"/>
        <html>
            <xsl:comment>Created by transformation scenario "synopsis-table-html.xsl" on <xsl:value-of select="$timestamp"/>!</xsl:comment>
            <head>
                <meta charset="utf-8"/>
                <!--<style>
                    <xsl:copy-of select="unparsed-text('styles.css')"></xsl:copy-of>
                </style>-->
                <link rel="stylesheet" href="assets/css/tables.css"></link> 
            </head>
            <body>
                <xsl:apply-templates select="synopsis/groups"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="groups">
        <table class="groups" width="100%">
            <tr>
                <th></th>
                <xsl:for-each select="$texts">
                    <xsl:variable name="text-uri" select="."/>
                    <xsl:variable name="title" select="$text-nodes[@href = $text-uri]/title"/>
                    <th><xsl:value-of select="$title"/></th>
                </xsl:for-each>
            </tr>
            <xsl:apply-templates/>
        </table>
    </xsl:template>
    
    <xsl:template match="group">
        <xsl:variable name="group" select="."/>
        <tr class="group" data-n="{@n}">
            <th class="group-label"><xsl:value-of select="@n"/></th>
            <xsl:for-each select="$texts">
                <xsl:variable name="text-uri" select="."/>
                <xsl:choose>
                    <xsl:when test="$group/item[@href = $text-uri][@ref]">
                        <xsl:apply-templates select="$group/item[@href = $text-uri][@ref]"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <td data-uri="{$text-uri}" class="item empty">-</td>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </tr>
    </xsl:template>
    
    <xsl:template match="item">
        <td class="item" data-uri="{@href}" data-ref="{@ref}">
            <xsl:apply-templates mode="includes"/>
        </td>
    </xsl:template>
    
    
    <!-- INCLUDES -->
    
    <xsl:template match="element()" mode="includes">
        <xsl:element name="{string-join(('tei', name()), '-')}">
            <xsl:apply-templates select="@*|node()" mode="includes"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="@*" mode="includes">
        <xsl:attribute name="data-{local-name()}">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>
    
</xsl:stylesheet>