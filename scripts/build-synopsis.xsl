<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:syn="https://sub.uni-goettingen.de/met/standards/synopsis#"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    exclude-result-prefixes="xs"
    version="2.0" 
    xpath-default-namespace="https://sub.uni-goettingen.de/met/standards/synopsis#"
    xmlns="https://www.w3.org/1999/xhtml/">
    
    <!--<xsl:output indent="yes"/>-->
    
    <xsl:variable name="text-nodes" select="//texts/text"/>
    <xsl:variable name="texts" select="data(//texts/text/@href)"/>
    <xsl:variable name="base-uri" select="base-uri()"/>
    
    <xsl:template match="/">
        <xsl:apply-templates />
    </xsl:template>
    
    <xsl:template match="synopsis">
        <xsl:variable name="timestamp" select="current-dateTime()"/>
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:comment>Created by transformation scenario "build-synopsis.xsl" on <xsl:value-of select="$timestamp"/>!</xsl:comment>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="item[xi:include]">
        <xsl:copy>
            <xsl:apply-templates select="@*|xi:include"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="xi:include">
        <xsl:variable name="xpointer" select="@xpointer"/>
        <xsl:variable name="pwd" select="substring-before($base-uri, tokenize($base-uri, '/')[last()])"/>
        <xsl:variable name="file-uri" select="concat($pwd, @href)"/>
        <xsl:apply-templates select="doc($file-uri)//node()[@xml:id = $xpointer]" mode="includes"/>
        <!--<xsl:value-of select="$file-uri"/>-->
    </xsl:template>
    
    <xsl:template match="item[@expanded = 'true']">
        <xsl:copy-of select="."/>
    </xsl:template>
    
    <xsl:template match="item[@href][@ref][not(@expanded = 'true')]">
        <xsl:variable name="xpointer" select="@ref"/>
        <xsl:variable name="pwd" select="substring-before($base-uri, tokenize($base-uri, '/')[last()])"/>
        <xsl:variable name="file-uri" select="concat($pwd, @href)"/>
        <xsl:copy>
            <xsl:attribute name="expanded" select="true()"/>
            <xsl:apply-templates select="@*[not(name() = 'expanded')]"/>
            <xsl:copy-of select="doc($file-uri)/id($xpointer)"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="item[@text][@ref][not(@expanded = 'true')]">
        <xsl:variable name="xpointer" select="@ref"/>
        <xsl:variable name="pwd" select="substring-before($base-uri, tokenize($base-uri, '/')[last()])"/>
        <xsl:variable name="href" select="id(@text)/@href => data()"/>
        <!--<test>
            <xsl:value-of select="@text"/>
            <xsl:value-of select="id(@text)/@href"/>
        </test>-->
        <xsl:variable name="file-uri" select="concat($pwd, $href)"/>
        <xsl:copy>
            <xsl:attribute name="expanded" select="true()"/>
            <xsl:if test="not(@href)">
                <xsl:attribute name="href" select="$href"/>
            </xsl:if>
            <xsl:apply-templates select="@*[not(name() = 'expanded')]"/>
            <xsl:copy-of select="doc($file-uri)/id($xpointer)"/>
        </xsl:copy>
    </xsl:template>
    
    
    <!-- INCLUDES -->
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>